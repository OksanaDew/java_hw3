package ua.danit.fs11;

import java.util.Scanner;

public class WeekTaskScheduler {

    public static final String MONDAY = "monday";
    public static final String TUESDAY = "tuesday";
    public static final String WEDNESDAY = "wednesday";
    public static final String THURSDAY = "thursday";
    public static final String FRIDAY = "friday";
    public static final String SATURDAY = "saturday";
    public static final String SUNDAY = "sunday";

    public static void main (String[] args){

        String[][] schedule = new String[7][2];

        schedule[0][0] = SUNDAY;
        schedule[0][1] = "do home work";
        schedule[1][0] = MONDAY;
        schedule[1][1] = "go to courses";
        schedule[2][0] = TUESDAY;
        schedule[2][1] = "shopping trip";
        schedule[3][0] = WEDNESDAY;
        schedule[3][1] = "watch a film";
        schedule[4][0] = THURSDAY;
        schedule[4][1] = "cook eat";
        schedule[5][0] = FRIDAY;
        schedule[5][1] = "meeting with friends";
        schedule[6][0] = SATURDAY;
        schedule[6][1] = "apartment cleaning";

        Scanner scanner = new Scanner(System.in);
        int stopWhileCycle = 0;

        while (stopWhileCycle == 0){

            System.out.println("Please, input the day of the week");
            String answer = scanner.next();

            switch (answer.toLowerCase()) {
                case (SUNDAY):
                    System.out.println("Your tasks for Sunday: " + schedule[0][1]);
                    break;
                case (MONDAY):
                    System.out.println("Your tasks for Monday: " + schedule[1][1]);
                    break;
                case (TUESDAY):
                    System.out.println("Your tasks for Tuesday: " + schedule[2][1]);
                    break;
                case (WEDNESDAY):
                    System.out.println("Your tasks for Wednesday: " + schedule[3][1]);
                    break;
                case (THURSDAY):
                    System.out.println("Your tasks for Thursday: " + schedule[4][1]);
                    break;
                case (FRIDAY):
                    System.out.println("Your tasks for Friday: " + schedule[5][1]);
                    break;
                case (SATURDAY):
                    System.out.println("Your tasks for Saturday: " + schedule[6][1]);
                    break;
                case ("exit"):
                    stopWhileCycle++;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    break;
            }
        }
    }
}
